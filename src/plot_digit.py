#!/usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" A function to plot a few test or train digits along with their labels.

- Inspired by http://scikit-learn.org/stable/auto_examples/classification/plot_digits_classification.html


- *Pylint:* Your code has been rated at 9.67/10.
- *Date:* Thursday 25 February 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
import matplotlib.pyplot as plt
from time import sleep


def plot_digits(images, labels, test=False, k=28,
                features=False, name_features="HoG"):
    """ Plotting the digits in images, numbered with labels. """
    # plt.hold(True)
    t = len(images)
    print("Plotting {} digits ...".format(t))
    images_and_labels = list(zip(images, labels))
    for index, (image, label) in enumerate(images_and_labels):
        offset = t if (test or features) else 0
        ax = plt.subplot(2, t, index + 1 + offset)
        ax.axis('off')
        ax.imshow(image.reshape((k, k)), cmap='gray', interpolation='none')
        if test:
            ax.set_title('Prediction: {}'.format(label))
        elif features:
            ax.set_title('{}: {}'.format(name_features, label))
        else:
            ax.set_title('Training: {}'.format(label))
    try:  # Try to maximize the window
        wm = plt.get_current_fig_manager()
        wm.window.state('zoomed')
    except Exception as e:
        print("Exception: {}. Failed to zoom the window.".format(e))
    # plt.show()


# Not used
def plot_all(images, labels, deltat=0.5):
    """ Plot all the images,labels, 10 by 10, timely spaced by deltat seconds. """
    j = 0
    t = 10
    plt.figure(4)
    for _ in range(len(images)/t):
        plot_digits(images[j:j+t], labels[j:j+t])
        sleep(deltat)

# End of plot_digit.py
