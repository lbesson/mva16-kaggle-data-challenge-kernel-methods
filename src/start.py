#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" Our Python 2/3 script for the MVA Kernel Methods Digit Classification Kaggle challenge
(https://inclass.kaggle.com/c/kernel-methods-in-machine-learning-mva).

Details:
  - Using the data similar to the MNIST database, from https://inclass.kaggle.com/c/kernel-methods-in-machine-learning-mva/data/
  - See our report for more details about the workflow, and see documentation/comments in the Python programs for more details about our implementation.


References:

  - Handwritten digit classification using the MNIST data set,
    M. Wu, Z. Zhang -- Technique report of course project for CSE802
    (https://www.researchgate.net/profile/Ming_Wu23/publication/228685853_Handwritten_Digit_Classification_using_the_MNIST_Data_Set/links/5409c76b0cf2f2b29a2c57b5.pdf)

  - Handwritten digit recognition: benchmarking of state-of-the-art techniques,
    CL. Liu, K. Nakashima, H. Sako, H. Fujisawa -- Pattern Recognition, 2003 -- Elsevier
    (https://www.researchgate.net/profile/Hiromichi_Fujisawa/publication/222834590_Handwritten_digit_recognition_Benchmarking_of_state-of-the-art_techniques/links/00463521bf24999020000000.pdf)


Results:
  - See the leaderboard (https://inclass.kaggle.com/c/kernel-methods-in-machine-learning-mva/leaderboard),
  - 16th submission gave 94.320% ...
  - We were stuck below 95% ! Sad :-(


- *Pylint:* Your code has been rated at 10.00/10.
- *Date:* Wednesday 16th March 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
print("\nLaunching script, importing modules ...")
from time import time
start_time = time()  # Just to count the total running time at the end

import numpy as np
import matplotlib.pyplot as plt

# from sklearn.preprocessing import scale  # XXX Nope !
from scale import scale  # FIXED Use our implementation !

# from sklearn.decomposition import PCA  # XXX Nope !
from pca import myPCA as PCA  # FIXED Use our implementation !

# from sklearn.svm import SVC  # XXX Nope !
from svm import mySVC as SVC  # FIXED Use our implementation !

from svm import classification_score  # XXX Local hand-made module

from sklearn.metrics import classification_report  # XXX Nope !
print("[WARNING] The only use of scikit-learn is the sklearn.metrics.classification_report function, used only to print a classification report (nothing more).")

print("\nModules imported ...")


# Increasing the cache size if running on the zamok server
cache_size = 250  # Cache size, float, in MB.
try:
    from os import getenv
    from sys import version
    if (getenv('USER') == 'besson') and (version.find('Continuum') > 0):
        print("I detected that I am running on zamok (durty hack), so I increase the cache_size !")
        cache_size = 8000  # Cache size, float, in MB.
except ImportError:
    print("Using default cache size.")
print("Using a cache size for the SVM classifier of", cache_size, "...")


# The competition datafiles are in the directory ../data/
print("Reading train/test data from ../data/ ...")
print("[WARNING] It might fail on non-Linux based OS (Windows mainly).")
print("(It might take a few seconds)")


# False to use the Kaggle data, True to the complete MNIST database
USE_COMPLETE_MNIST = False

# Read competition data files:
if USE_COMPLETE_MNIST:
    print("Loading the complete MNIST database... (42000 train, 28000 test) ...")
    print("(From the other Kaggle challenge https://www.kaggle.com/c/digit-recognizer/ )")
    data = np.loadtxt("../data.big/train.csv", delimiter=',', skiprows=1, dtype=float)
    train = data[:, 1:]
    target = np.array(data[:, 0], dtype=int)
    test = np.loadtxt("../data.big/test.csv", delimiter=',', skiprows=1, dtype=float)
else:
    print("Loading the MVA Kaggle database... (5000 train, 10000 test) ...")
    train = np.loadtxt("../data/Xtr.csv", delimiter=',', skiprows=0, dtype=float)  # There is no header line
    target = np.loadtxt("../data/Ytr.csv", delimiter=',', skiprows=1, dtype=int)  # There is ONE header line ("Id,Label" in this data file)
    target = target[:, 1]  # Only keep the label, not the index
    test = np.loadtxt("../data/Xte.csv", delimiter=',', skiprows=0, dtype=float)  # There is no header line
# Done reading the data


print("There is %i train data point (28 x 28 gray-scaled image)." % len(train))
print("    - shape of train:", train.shape)    # 5000 × 784  (5000,784)
print("    - shape of target:", target.shape)  # 5000 × 1    (5000,)

settarget = set(target)
print("Set of target labels:", settarget)
assert settarget == set(range(10)), "[ERROR] Set of target labels is not {0,..,9} !"
print("Histogram of labels:")
print([sum(target == i) for i in list(settarget)])

print("There is %i test data point (28 x 28 gray-scaled image)." % len(test))
print("    - shape of test:", test.shape)      # 10000 × 784 (10000,784)


# Display an histogram of the labels, to check that they are uniformly distributed
# Do it only once, set False to True to display it and save it again
if False and __name__ == '__main__':
    K = 10
    fig = plt.figure(1)
    plt.hold(True)
    fig.suptitle("Repartition of the labeled images (digits in the train dataset), mean in red", fontsize=18)
    bins = plt.hist(target, color='darkgreen')[0]
    m = np.mean(bins)
    plt.plot(np.arange(0, K, 1), [m]*K, 'r-', lw=5)  # Also plot the mean value, ~= 500
    plt.xlim([0, K-1])
    plt.xticks(np.arange(0, K, 1))
    plt.xlabel("Digit 0, 1, ..., %i" % (K-1))
    plt.ylabel("Number of train data for this digit")
    plt.yticks(np.arange(0, 650, 50))  # XXX not generic
    # plt.savefig("train_data__histogram_of_labels.png")  # XXX uncomment to save the plot
    plt.show()


# Display a few numbers
t = 10  # How many numbers to display
i0 = 200  # Index from which to start

from plot_digit import plot_digits  # XXX Local hand-made module

# Do it only once, set False to True to display it and save it again
# if True and __name__ == '__main__':
if False and __name__ == '__main__':
    fig = plt.figure(2)
    plt.hold(True)
    fig.suptitle("A few train and test digits, before training.", fontsize=18)
    plot_digits(train[i0:i0+t], target[i0:i0+t])  # Train
    plot_digits(test[i0:i0+t], ['?'] * t, test=True)  # Test
    # plt.savefig("../fig/example_10_train_test_points_200-210.png")
    plt.show()


# Scale the input data (center, reduce)
# XXX After many experiments, it really seems to NOT be a good idea to center,reduce
USE_SCALE = False
USE_SCALE = True

if USE_SCALE:
    print("Pre-processing the data, with scale(): centering so mu = 0, scaling so std = 1 ...")
    train_all = scale(train, copy=True)
    test_all = scale(test, copy=True)
else:
    print("No pre-processing the data, not using scale() ...")
    print("train.mean =", train.mean(), "and train.std =", train.std())
    train_all = train.copy()
    print("test.mean =", test.mean(), "and test.std =", test.std())
    test_all = test.copy()

# Target are in {0,..,9}, no scalling to do
target_all = target.copy()


# To try fancy features or filters
USE_FANCY_FEATURES = False


# Clever features extraction
def apply_each_image(X, extract_features, keep_raw=False):
    """ Apply a feature extraction function on each image of the (n, wh) shaped vector X.

    - keep_raw=True to also keep the raw pixels, additionnally to the extracted features.
    """
    n, wh = np.shape(X)
    w = h = int(np.sqrt(wh))
    if wh != w * h:
        raise ValueError("[ERROR] apply_each_image: non-squared image! wh = {}, w = {}, h = {} ...".format(wh, w, h))
    print("apply_each_image with", extract_features.__name__, ": n =", n, "and w =", w, "and h =", h)
    raw_images = list(np.reshape(X, (n, w, h)))
    # print("shape of raw_images:", np.shape(raw_images))
    transformed_images = np.array([extract_features(raw_images[i]) for i in range(n)])
    # print("shape of transformed_images:", np.shape(transformed_images))
    transformed_X = np.reshape(transformed_images, (n, w*h))
    # print("shape of transformed_X", np.shape(transformed_X))
    if keep_raw:
        # res = np.hstack((X, transformed_X))  # XXX First X then features ?
        res = np.hstack((transformed_X, X))  # XXX First features then X ?
        print("  Keeping the raw pixels as features also (in the end of the vector) ...")
    else:
        res = transformed_X
        print("  Not keeping the raw pixels as features, only the extracted features ...")
    print("  Shape of the result :", np.shape(res))
    return res


if USE_FANCY_FEATURES:
    # DONE We tried the directional Sobel 3x3 filters
    from skimage.filters import sobel  # XXX to write myself!

print("\n[WARNING] The scikit-image module was used to try filters and features extraction, but we do not use any of these fancy features at the end.")


def sobel_each_image(X):
    """ Apply the 3x3 Sobel directional filter on each image of the (n, wh) shaped vector X. """
    return apply_each_image(X, sobel)


# DONE We tried Histogram of Gradiens, http://research.ijcaonline.org/volume104/number9/pxc3899167.pdf
# Example and documentation here http://scikit-image.org/docs/dev/auto_examples/plot_hog.html
if USE_FANCY_FEATURES:
    from skimage.feature import hog  # XXX to write myself!
    from skimage.exposure import rescale_intensity
    USE_RESCALE_INTENSITY = True

# XXX Parameters for the HoG feature extraction
# Seems to be the best, 91.4% on the 2/3 - 1/3 train-train split
orientations = 9  # Nb of gradient orientations, 2 = Sobel, 4, 8 12 or 16, 16 seems to be the best
pixels_per_cell = (14, 14)  # Nb of pixel per cell, for a 28x28 image, that's 2x2 cells = 4 cells per image
cells_per_block = (2, 2)  # XXX ??


def hog_one_image(image):
    """ Apply the HoG feature extraction on an image of the (wh,) shaped vector X. """
    _, hog_image = hog(image,
                       orientations=orientations, pixels_per_cell=pixels_per_cell,
                       cells_per_block=cells_per_block, visualise=True)
    if USE_RESCALE_INTENSITY:  # Rescale histogram for better display / more uniform HoG features
        hog_image = rescale_intensity(hog_image, in_range=(0, 1))  # (0, 0.02)
    return hog_image


def hog_each_image(X):
    """ Apply the HoG feature extraction on each image of the (n, wh) shaped vector X. """
    return apply_each_image(X, hog_one_image, keep_raw=False)
    # return apply_each_image(X, hog_one_image, keep_raw=True)


# XXX We should add the direction detection features
USE_SOBEL = True
USE_SOBEL = False
USE_HOG = True
USE_HOG = False


# XXX we have to add clever features
if USE_SOBEL:
    print("Using clever features: 3x3 Sobel directional features ...")
    train_all = sobel_each_image(train_all)
    test_all = sobel_each_image(test_all)
elif USE_HOG:
    print("Using clever features: HoG directional features ...")
    print("Parameters:",
          "orientations =", orientations,
          "pixels_per_cell =", pixels_per_cell,
          "cells_per_block =", cells_per_block,
          "visualise =", True)
    train_all = hog_each_image(train_all)
    test_all = hog_each_image(test_all)
    print("    - shape of train_all:", train_all.shape)    # 5000 × 1568  (5000,1568)
    print("    - shape of test_all:", test_all.shape)      # 10000 × 1568 (10000,1568)
else:
    print("Not using clever features ...")


# XXX To experiment new models quickly, on only 500,1000 train,test points
SUBSAMPLE = True
SUBSAMPLE = False

if SUBSAMPLE:
    # Only select a small of data
    test_size = 2.0/3.0
    n_samples = 1+int((1-test_size) * len(target))
    print("Only using a subsample of train data: just {:%} for training ...".format(test_size))
    try:
        from sklearn.cross_validation import train_test_split
        print("[WARNING] From scikit-learn we just use the sklearn.cross_validation.train_test_split function, used only to randomized and ensure consistency for the train_train/train_test splitting (not involved in the final prediction).")
        train, _, target, _ = train_test_split(train_all, target_all, test_size=test_size)
    # XXX Ensure pseudo equi-repartition between classes
    except ImportError:
        print("[WARNING] sklearn.cross_validation.train_test_split function was not available, using a non-random train_train/train_test splitting (not involved in the final prediction).")
        train = train_all[:n_samples]
        target = target_all[:n_samples]
    print("Only using a subsample of test data: just the first %i lines for training ..." % (n_samples*2))
    test = test_all[:n_samples*2]
else:
    print("Using all the data... It will take a while... Be patient !")
    train = train_all
    target = target_all
    test = test_all
    n_samples = len(train)

# Check the shapes
print("    - shape of train:", train.shape)
print("    - shape of target:", target.shape)
print("    - shape of test:", test.shape)


# XXX Experimental : center,reduce AFTER having extracted the features?
USE_SCALE_AFTER_FEATURES = False
USE_SCALE_AFTER_FEATURES = True

if USE_SCALE_AFTER_FEATURES:
    print("Post-processing the data, calling scale() on the transformed data: centering so mu = 0, scaling so std = 1 ...")
    train = scale(train, copy=True)
    test = scale(test, copy=True)
else:
    print("No post-processing the data, not calling scale() on the transformed data ...")


# XXX To use or don't use a PCA dimensionality reduction
USE_PCA = True
USE_PCA = False

if USE_PCA:
    # Work well with 30 <= n_components <= 60, 40 seems enough
    n_components = 80  # 30, 70, 100  # XXX

    print("Creating the PCA transform, with %i components..." % n_components)
    # pca_model = PCA(n_components=n_components, copy=False, whiten=True)
    pca_model = PCA(n_components=n_components)

    print("Transforming train/test data by this PCA ...")
    train = pca_model.fit_transform(train)
    test = pca_model.transform(test)

    print("Now the train/test data have these shapes:")
    print(train.shape)
    print(test.shape)


# if True and (not USE_PCA) and __name__ == '__main__':
if False and (not USE_PCA) and __name__ == '__main__':
    print("Plotting of some train digits, and HoG features ...")
    fig = plt.figure(5)
    plt.hold(True)
    fig.suptitle("Example of HoG filters", fontsize=18)
    X = train[i0:i0+t]
    plot_digits(X, target[i0:i0+t])  # Train no HoG
    X = hog_each_image(X)
    plot_digits(X, target[i0:i0+t], features=True)  # Train with HoG
    # plt.savefig("../fig/example_10_train_HoG_points_200-210.png")
    plt.show()


# Parameter for the SVC. DONE: explore !

# Regularization parameter, 1.0 seems fine
# WARNING Risk of underfitting if too small, and risk of overfitting if too big
C = 1.0  # Default value
C = 5.5  # XXX Seems to be the best, by grid search

# Chosen kernel, can be 'linear', 'poly', 'rbf', 'sigmoid', or custom
# kernel = 'sigmoid'  # XXX Work really badly here
# kernel = 'laplace'  # XXX Work really badly here too
kernel = 'linear'  # Work well here, for HoG filters
kernel = 'poly'  # Work well with degree >= 3, not better for higher degree
kernel = 'rbf'  # Work the best, seems better than 'poly'
# XXX try a custom kernel ? (more adapted to the digits ?) No, work on the features instead!

# Degree, only for 'poly' kernel, degree 4 seems to work well
# XXX except for digits 8 and 9
degree = 3

# Kernel coefficient, for the other kernels
# gamma should be of the order of the variance of the samples
if USE_SCALE or USE_SCALE_AFTER_FEATURES:
    gamma = 'auto'  # 1 / n_features, so will be small
gamma = 0.001  # XXX Seems to be the best, by grid search

# Intercept coefficient (offset), for 'poly' kernel
coef0 = 1.0 if kernel == 'poly' else 0.0

# Liste des paramètres à donner au classifieur SVC
svm_parameters = {'C': C,
                  'kernel': kernel,
                  'gamma': gamma,
                  'degree': degree,
                  'coef0': coef0,
                  'cache_size': cache_size}
print("Using these parameters for the SVM classifier:", svm_parameters)


#
# 1. Vérifications et tests sur 2/3 des donneés de train
#

# 1.1. Vérification sur 2/3 des données de train
NB_FIRST_CHECK = 1
NB_FIRST_CHECK = 0

if NB_FIRST_CHECK > 0:
    from sklearn.cross_validation import train_test_split
    scores = np.zeros(NB_FIRST_CHECK)
    print("\n\n1. Learn on 2/3 and evaluate the performance on 1/3 of the train data ...")
    print("As for real train/test, we have twice as much as train as we have of test.")
    test_size = 1.0/3.0

    for nbcheck in range(NB_FIRST_CHECK):
        print("\n  - Check number {} : creating svm_model, splitting randomly train data, training it and checking score on the rest of the train data ...".format(1+nbcheck))
        # Create a classifier: a support vector classifier
        svm_model = SVC(**svm_parameters)  # The ** is just a fancy way to pass the arguments, unpacking them from the dictionary svm_parameters

        # Add a randomization in this train/test splitting part
        X_train, X_test, y_train, y_test = train_test_split(train, target, test_size=test_size)
        # We learn the digits on the first 2/3 of the digits
        svm_model.fit(X_train, y_train)
        # Now predict the value of the digit on the second 1/3
        predicted = svm_model.predict(X_test)

        print("Classification report for svm_model %s:\n%s\n" % (svm_model, classification_report(y_test, predicted)))
        half_train_score = scores[nbcheck] = classification_score(y_test, predicted)
        print("Checking the score on 1/3 of the train data : {:.2%} ...".format(half_train_score))
        if half_train_score < 0.85:
            print("WARNING : half_train_score < 85% might indicate that something went wrong (underfitting)")
        elif half_train_score > 0.97:
            print("WARNING : half_train_score > 97% might indicate that something went wrong (overfitting)")
    # End
    print("\nAfter {} tries, the average score is: {:.3%} +/- {:.3%} ...".format(NB_FIRST_CHECK, np.mean(scores), 2 * np.std(scores)))


# 1.2. Try an Ensemble method
# http://scikit-learn.org/dev/modules/generated/sklearn.ensemble.BaggingClassifier.html
USE_ENSEMBLE = True
USE_ENSEMBLE = False

if USE_ENSEMBLE:
    from sklearn.cross_validation import train_test_split
    from sklearn.ensemble import BaggingClassifier
    n_estimators = 5
    print("\nTrying an Ensemble method, BaggingClassifier with", n_estimators, "classifiers ...")
    # Create an aggregated classifier: a support vector classifier
    bagged = BaggingClassifier(SVC(**svm_parameters),
                               max_samples=0.85, max_features=1.0,
                               n_estimators=n_estimators, n_jobs=(-1), verbose=2,
                               bootstrap=False)
    # The ** is just a fancy way to pass the arguments, unpacking them from the dictionary svm_parameters
    # Add a randomization in this train/test splitting part
    test_size = 1.0/3.0
    X_train, X_test, y_train, y_test = train_test_split(train, target, test_size=test_size)
    # We learn the digits on the first half of the digits
    bagged.fit(X_train, y_train)
    # Now predict the value of the digit on the second half
    predicted = bagged.predict(X_test)
    setpredicted = set(predicted)
    print("Set of predicted labels:", setpredicted)
    print("Histogram of labels:")
    print([sum(target == i) for i in list(setpredicted)])
    print("Classification report for bagged %s:\n%s\n" % (bagged, classification_report(y_test, predicted)))
    half_train_score = classification_score(y_test, predicted)
    print("Score on half of the train data : {:.2%} ...".format(half_train_score))


# 1.3. Try an Voting method
# http://scikit-learn.org/dev/modules/generated/sklearn.ensemble.VotingClassifier.html
USE_VOTING = True
USE_VOTING = False

if USE_VOTING:
    from sklearn.ensemble import VotingClassifier
    print("\nTrying an Ensemble method, VotingClassifier with 3 different classifiers ...")
    clf1 = SVC(kernel='rbf', gamma=0.05, cache_size=cache_size)
    print("  1. SVC, RBF, gamma = 0.05 ...")
    clf2 = SVC(kernel='rbf', gamma='auto', cache_size=cache_size)
    print("  2. SVC, RBF, gamma = 'auto' ...")
    clf3 = SVC(kernel='poly', degree=2, cache_size=cache_size)
    print("  3. SVC, poly, degree = 2 ...")
    clf4 = SVC(kernel='poly', degree=3, cache_size=cache_size)
    print("  4. SVC, poly, degree = 3 ...")
    clf5 = SVC(kernel='linear', cache_size=cache_size)
    print("  5. SVC, linear ...")
    # Aggregate these 4 SVC, by a VotingClassifier
    eclf = VotingClassifier(estimators=[('rbf-0.05', clf1), ('rbf-auto', clf2), ('poly-2', clf3), ('poly-3', clf4), ('linear', clf5)], voting='hard')
    n_split = n_samples // 2
    # We learn the digits on the first half of the digits
    eclf.fit(train[:n_split], target[:n_split])
    expected = target[n_split:]
    # For each classifier
    for c, name in zip([clf1, clf2, clf3, clf4, clf5, eclf],
                       ["SVC, RBF, gamma = 0.05", "SVC, RBF, gamma = 'auto'", "SVC, poly, degree = 2", "SVC, poly, degree = 3", "SVC, linear", "Voting aggregated classifier"]):
        print("- For clf {}, training it ...".format(name))
        c.fit(train[:n_split], target[:n_split])
        half_train_score = c.score(train[n_split:], expected)
        print("- For clf {}, score on half of the train data is {:.2%} ".format(name, half_train_score))
    # Now predict the value of the digit on the second half
    predicted = eclf.predict(train[n_split:])
    print("Classification report for aggregated eclf %s:\n%s\n" % (eclf, classification_report(expected, predicted)))


# 1.4. Try a grid search
# http://scikit-learn.org/stable/auto_examples/model_selection/grid_search_digits.html#example-model-selection-grid-search-digits-py
USE_GRID_SEARCH = True
USE_GRID_SEARCH = False

if USE_GRID_SEARCH:
    print("\nTrying an grid search, GridSearchCV with 5-fold cross validation ...")
    print("[WARNING] It takes a LOT OF TIME (20-30 minutes) ...")
    from sklearn.grid_search import GridSearchCV
    # Set the parameters by cross-validation
    # grid_C = [1, 2, 3, 4, 4, 5, 6, 7.5]
    grid_C = [3.8, 3.9, 4, 4.1, 4.2]
    # grid_gamma = ['auto', 1e-2, 1e-3, 1e-4]
    grid_gamma = [2.5e-3, 1e-3, 7.5e-4]
    tuned_parameters = [
        # {'kernel': ['linear'], 'C': [1]},  # Simply the usual linear kernel
        {'kernel': ['rbf'], 'gamma': grid_gamma, 'C': grid_C},
        # {'kernel': ['poly'], 'degree': [2, 3, 4, 5, 6], 'C': grid_C}
    ]
    print("Exploring these parameters:")
    print(tuned_parameters)
    # 2 different measures of success
    # precision_measure = ['precision', 'recall']
    precision_measure = ['precision']
    for score in precision_measure:
        print("\n- Tuning hyper-parameters for", score, "... It will take a LOT OF TIME ...")
        clf = GridSearchCV(SVC(cache_size=cache_size), tuned_parameters, cv=3,
                           scoring='%s_weighted' % score,
                           n_jobs=6,
                           verbose=2)
        clf.fit(X_train, y_train)

        print("Best parameters set found on development set:\n")
        print(clf.best_params_)
        print("Grid scores on development set:\n")
        for params, mean_score, scores in clf.grid_scores_:
            print("%0.3f (+/- %0.03f) for %r"
                  % (mean_score, scores.std() * 2, params))

        print("\nDetailed classification report:\n")
        print("The model is trained on the full development set.")
        print("The scores are computed on the full evaluation set.\n")
        y_true, y_pred = y_test, clf.predict(X_test)
        print(classification_report(y_true, y_pred))
        print("Setting the parameters according to the best found ...")
        svm_parameters.update(clf.best_params_)
        try:
            kernel = clf.best_params_['kernel']
        except KeyError:
            print("  [WARNING] Unable to update the variable 'kernel' according to clf.best_params_['kernel'] ...")
        try:
            gamma = clf.best_params_['gamma']
        except KeyError:
            print("  [WARNING] Unable to update the variable 'gamma' according to clf.best_params_['gamma'] ...")
        try:
            C = clf.best_params_['C']
        except KeyError:
            print("  [WARNING] Unable to update the variable 'C' according to clf.best_params_['C'] ...")
        print("New svm_parameters is:\n", svm_parameters)


# # FIXED Exit now
# if True and __name__ == '__main__':
if False and __name__ == '__main__':
    from sys import exit as sysexit
    print("Only testing, remove these lines to train/predict on the all dataset ...")
    sysexit(1)

#
# 2. Vraie prédiction sur toutes les données
#
print("\n\n2. Training on all the %i train data points and predicting :" % n_samples)
print("Creating a SVM classifier ...")

# Créé le prédicteur SVM
svm_model = SVC(**svm_parameters)  # The ** is just a fancy way to pass the arguments, unpacking them from the dictionary svm_parameters

USE_ENSEMBLE = True
USE_ENSEMBLE = False
if USE_ENSEMBLE:
    from sklearn.ensemble import BaggingClassifier
    n_estimators = 6
    print("\nUsing an Ensemble method, BaggingClassifier with", n_estimators, "classifiers ...")
    svm_model = BaggingClassifier(SVC(**svm_parameters),
                                  max_samples=0.80, max_features=1.0,
                                  n_estimators=n_estimators, n_jobs=(-1), verbose=2,
                                  bootstrap=False)
print(svm_model)

# 2.1. Entraîne le prédicteur sur les données connues (étiquetées)
print("Training the SVM on the train/target data ...")
svm_model.fit(train, target)

# Vérifie son score sur ces mêmes données connues (ne doit pas être trop "parfait", sinon on risque d'overfit)
CHECK_SCORE_TRAIN_DATA = False
CHECK_SCORE_TRAIN_DATA = True
if CHECK_SCORE_TRAIN_DATA:
    train_score = svm_model.score(train, target)
    print("Checking the score on the train data : {:.2%} ...".format(train_score))
    if train_score < 0.85:
        print("WARNING : train_score < 85% might indicate that something went wrong (underfitting)")
    elif train_score > 0.97:
        print("WARNING : train_score > 97% might indicate that something went wrong (overfitting)")

# Nombre de vecteurs supports
try:
    print("This SVM classifier has these support vectors (class-wise):", svm_model.n_support_)
except AttributeError:
    print("This aggregated SVM classifier does not have an attribute .n_support_ ...")

# 2.2. Prédire sur les données non-étiquetées
print("Then predicting on the test data ...")
prediction = svm_model.predict(test)

# Vérifier qu'on a le bon nombre de prédiction.
print("Shape of the prediction:", prediction.shape)


# 2.3. Plot a few predictions

# if True and (not USE_PCA) and __name__ == '__main__':
if False and (not USE_PCA) and __name__ == '__main__':
    print("Plotting a few train and test digits, after training ...")
    fig = plt.figure(3)
    plt.hold(True)
    fig.suptitle("A few train and test digits, after training.", fontsize=18)
    plot_digits(train[i0:i0+t], target[i0:i0+t])  # Train
    plot_digits(test[i0:i0+t], prediction[i0:i0+t], test=True)  # Test
    plt.show()

# if True and (not USE_PCA) and __name__ == '__main__':
if False and (not USE_PCA) and __name__ == '__main__':
    print("Plotting Sobel of some train digits, and test digits, after training ...")
    fig = plt.figure(4)
    plt.hold(True)
    fig.suptitle("Test of Sobel filters", fontsize=18)
    X = sobel_each_image(train[i0:i0+t])
    plot_digits(X, target[i0:i0+t])  # Train
    plot_digits(test[i0:i0+t], prediction[i0:i0+t], test=True)  # Test
    plt.show()

# if True and (not USE_PCA) and __name__ == '__main__':
if False and (not USE_PCA) and __name__ == '__main__':
    print("Plotting HoG of some train digits, and test digits, after training ...")
    fig = plt.figure(5)
    plt.hold(True)
    fig.suptitle("Example of HoG filters", fontsize=18)
    plot_digits(train[i0:i0+t], target[i0:i0+t])  # Train no HoG
    X = hog_each_image(train[i0:i0+t])
    # plot_digits(X, target[i0:i0+t])  # Train with HoG
    plot_digits(test[i0:i0+t], prediction[i0:i0+t], test=True)  # Test
    plt.show()


# XXX make the file name unique so we keep them all
if kernel == 'linear':
    kernel_name = 'linear'
elif kernel == 'poly':
    kernel_name = 'poly_d%i' % degree
elif kernel == 'rbf':
    kernel_name = 'RBF_g%s' % str(gamma)
else:
    kernel_name = kernel

# XXX add every simulation parameter in the filename
outname = 'Submission%s%s%s_SVM__%s_C=%s__Besson_Ralle_Roquero.csv' % (
    ('_PCA_%i' % n_components) if USE_PCA else '',
    '_Sobel' if USE_SOBEL else '',
    '_HoG' if USE_HOG else '',
    kernel, str(C)
    )

# Sauvegarde les prédictions dans le fichier texte 'outname'
print("And saving the prediction to the CSV file '%s' ..." % outname)

# Saving to 'outname', and to 'Yte.csv' (always have the final CSV file ready)
for on in [outname, 'Yte.csv']:
    np.savetxt(on,
               np.c_[range(1, len(test) + 1), prediction],
               delimiter=',',
               comments='',
               header='Id,Prediction',
               fmt='%d')

# On a fini !
print("Estimated running time {:g} seconds...".format(time() - start_time))
print("==> Done for this script start.py")

# End of start.py
