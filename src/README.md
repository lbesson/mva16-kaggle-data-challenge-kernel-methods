## Python programs for the Kaggle Data Challenge
> - **Team Besson, Ralle, Roquero**; written by Lilian Besson.
> - Written in [Python](https://www.python.org/), 2 or 3 (I always do my best in supporting both versions).

### [Dependencies](./requirements.txt)
> The following modules were used:

#### Generic scientific modules:
- [numpy](http://www.numpy.org/) and [scipy](http://www.scipy.org/) : numpy-array structures, scientific functions on array;
- [matplotlib](http://www.matplotlib.org/) : to plot the digits (we didn't plot any cross-validation success curves, or illustration of convergence for the optimizations);

#### *Optimization* module:
- [cvxopt](http://www.cvxopt.org/) : for their generic purpose [Quadratic Problem (QP) solver](http://cvxopt.org/userguide/coneprog.html#quadratic-programming) ([cvxopt.solvers.qp](http://cvxopt.org/userguide/coneprog.html#cvxopt.solvers.qp));

#### *Machine-learning* module:
- [scikit-learn](http://scikit-learn.org/) : **NOT** for their machine learning (SVM) algorithms, our final [start.py](./start.py) script only use scikit-learn for their ``classification_report`` function;

#### *Image processing* module:
- [scikit-image](http://scikit-image.org/) : **NOT** used in our final script, but used during the experimental process (as we tried the [Sobel filters](http://scikit-image.org/docs/dev/auto_examples/plot_edge_filter.html) and the [Histogram of Oriented Gradients](http://scikit-image.org/docs/dev/auto_examples/plot_hog.html))

----

### Organization
> - Each of these Python programs is very well documented, with inline comments (``# comments``) and docstring (``""" Documentation. """``).
> - It should be very clear what each variable is for, what each function/method does, and how to use each class.

#### Hand-made modules
- [scale.py](./scale.py) : for the scale function (center and reduce the train/test data);
- [pca.py](./pca.py) : implements a clone of the ``sklearn.PCA.pca`` class (a PCA transform with a ``.fit``, ``.transform`` and ``.fitransform`` methods);
- [plot_digit.py](./plot_digit.py) : implements a function ``plot_digits`` to display a dozen of train or test images;
- [SMO.py](./SMO.py) : I tried to implement the SMO algorithm, but without compilation or numpy-powered computation it turned out to be *really slow* (large scale iterative algorithm ==> slow);
- [kernels.py](./kernels.py) : implements the main kernel (RBF, Laplace, linear, polynomial, tanh), in a parametric way (like ``_rbf : (gamma, x, y) -> ``) and a functor way (like ``K = rbf(gamma) : (x, y) -> K(x, y)``, the functor is here to speed up computation);
- [svm.py](./svm.py) : implements the BinarySVC class (binary classifier based on SVM), and the mySVC class (« one-vs-all » strategy, based on BinarySVC). It can use either [cvxopt](http://cvxopt.org/)'s QP solver or [SMO.smo](./SMO.py) to solve the quadratic problem of learning/training the Support Vector Classifier (cvxopt.qp is *way quicker* than SMO.smo !). It is a clean and fully documented implementation, based on 2 Python classes. I tried to mimic the interface of the [sklearn.svm.SVM](http://scikit-learn.org/dev/modules/generated/sklearn.svm.SVC.html#sklearn.svm.SVC) class as well as possible (both sklearn's implementation and mine can be used with the same interface).

#### Scripts
- [test_svm_2D_data.py](./test_svm_2D_data.py) : is a script to try our implementation of a binary or 3- or 6-class SVM classifier, on *2D data* (to allow nice visualization). See the figures [in this folder](../fig/);
- [start.py](./start.py) : the main script, read the data from [this folder](../data/), transform or clean it, then train a 10-class SVC on the train data and predict the labels for the test data (and save it back to the [Yte.csv](./Yte.csv) file). Cf. the written report for more details about the mathematical ideas, and the densely commented for more details about the implementation.

----

### Evaluation of the code quality
[pylint](https://www.pylint.org/) says:

**Your code has been rated at 9.54/10**

*That's a pretty good sign that our code is clean and well written*
(not a sign that it works as expected, but [the CSV files](../csv/) and [the figures](../fig/) are here for this).

> [See the complete report](./pylint_report.md) or [each report individually](./pylint/) for more details.


----

### Source of inspiration
> These examples from scikit-learn or scikit-image were a source of inspiration at first:

- [plot_digits_linkage](http://scikit-learn.org/stable/auto_examples/cluster/plot_digits_linkage.html#example-cluster-plot-digits-linkage-py)
- [plot_digits_classification](http://scikit-learn.org/stable/auto_examples/classification/plot_digits_classification.html#example-classification-plot-digits-classification-py)
- [digits_classification_exercise](http://scikit-learn.org/stable/auto_examples/exercises/digits_classification_exercise.html#example-exercises-digits-classification-exercise-py)
- [plot_cv_digits](http://scikit-learn.org/stable/auto_examples/exercises/plot_cv_digits.html#example-exercises-plot-cv-digits-py)
- [plot_lle_digits](http://scikit-learn.org/stable/auto_examples/manifold/plot_lle_digits.html#example-manifold-plot-lle-digits-py)
- [plot_hog](http://scikit-image.org/docs/dev/auto_examples/plot_hog.html)

> This public Kaggle data challenge was used before the opening of the MVA « Kernel Methods » Kaggle :

- [www.kaggle.com/c/digit-recognizer/data](https://www.kaggle.com/c/digit-recognizer/data).

> These SVM implementations were also a source of inspiration:

- [M.Blondel's implementation of SVM in Python](http://www.mblondel.org/journal/2010/09/19/support-vector-machines-in-python/);
- [github.com/ajtulloch/svmpy](https://github.com/ajtulloch/svmpy/) (and [this article](http://tullo.ch/articles/svm-py/));
- [github.com/vickiniu/svm-python](https://github.com/vickiniu/svm-python/).

----

#### More ?
- See [the report](../report/) for explanations about this programs.
- See [the data](../data/), [the figures](../fig/) or [the references](../biblio/) if needed.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
