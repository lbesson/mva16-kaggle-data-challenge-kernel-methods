#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" A small script to test and plot our SVM implementation with 2D data.

- OK, test our BinarySVC implementation
- OK, test our SVC implementation with 2, 3 and 6 classes
- OK, test on 10 classes (not here, in the start.py)
- OK, test our SMO algorithm (it works, just REALLY slow)


References:
  - Inspiration from: http://www.mblondel.org/journal/2010/09/19/support-vector-machines-in-python/


- *Pylint:* Your code has been rated at 9.55/10.
- *Date:* Tuesday 15 March 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
print("\nLaunching script, importing modules ...")
from time import time
start_time_0 = time()  # Just to count the total running time at the end

import numpy as np
import pylab as pl


def gen_lin_separable_data():
    """ Generate training data in the 2-d case. """
    mean1 = np.array([0, 2])
    mean2 = np.array([2, 0])
    cov = np.array([[0.8, 0.6], [0.6, 0.8]])
    X1 = np.random.multivariate_normal(mean1, cov, 100)
    y1 = np.ones(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov, 100)
    y2 = np.ones(len(X2)) * (-1)
    return X1, y1, X2, y2


def gen_non_lin_separable_data():
    """ Generate non-linearly separable training data in the 2-d case. """
    mean1 = [-1, 2]
    mean2 = [1, -1]
    mean3 = [4, -4]
    mean4 = [-4, 4]
    cov = [[1.0, 0.8], [0.8, 1.0]]
    X1 = np.random.multivariate_normal(mean1, cov, 50)
    X1 = np.vstack((X1, np.random.multivariate_normal(mean3, cov, 50)))
    y1 = np.ones(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov, 50)
    X2 = np.vstack((X2, np.random.multivariate_normal(mean4, cov, 50)))
    y2 = np.ones(len(X2)) * (-1)
    return X1, y1, X2, y2


def gen_non_lin_separable_data_3class():
    """ Generate non-linearly separable training data in the 2-d case, 3 classes. """
    n = 100
    mean1 = [-1, 2]
    mean2 = [1, -1]
    cov1 = [[1.0, 0.8], [0.8, 1.0]]
    mean3 = [4, -4]
    mean4 = [-4, 4]
    cov2 = [[1.0, 0.75], [0.75, 1.0]]
    mean5 = [-5, -5]
    mean6 = [5, 5]
    cov3 = [[1.0, -0.8], [-0.8, 1.0]]
    X1 = np.random.multivariate_normal(mean1, cov1, n)
    X1 = np.vstack((X1, np.random.multivariate_normal(mean3, cov1, n)))
    y1 = np.zeros(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov2, n)
    X2 = np.vstack((X2, np.random.multivariate_normal(mean4, cov2, n)))
    y2 = np.zeros(len(X2)) + 1
    X3 = np.random.multivariate_normal(mean5, cov3, n)
    X3 = np.vstack((X3, np.random.multivariate_normal(mean6, cov3, n)))
    y3 = np.zeros(len(X3)) + 2
    return X1, y1, X2, y2, X3, y3


def gen_non_lin_separable_data_6class():
    """ Generate non-linearly separable training data in the 2-d case, 6 classes. """
    n = 200
    mean1 = [-1, 2]
    mean2 = [1, -1]
    cov1 = [[1.0, 0.8], [0.8, 1.0]]
    mean3 = [4, -4]
    mean4 = [-4, 4]
    cov2 = [[1.0, 0.75], [0.75, 1.0]]
    mean5 = [-5, -5]
    mean6 = [5, 5]
    cov3 = [[1.0, -0.8], [-0.8, 1.0]]
    X1 = np.random.multivariate_normal(mean1, cov1, n)
    y1 = np.zeros(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov2, n)
    y2 = np.zeros(len(X2)) + 1
    X3 = np.random.multivariate_normal(mean3, cov1, n)
    y3 = np.zeros(len(X3)) + 2
    X4 = np.random.multivariate_normal(mean4, cov2, n)
    y4 = np.zeros(len(X4)) + 3
    X5 = np.random.multivariate_normal(mean5, cov3, n)
    y5 = np.zeros(len(X5)) + 4
    X6 = np.random.multivariate_normal(mean6, cov3, n)
    y6 = np.zeros(len(X6)) + 5
    return X1, y1, X2, y2, X3, y3, X4, y4, X5, y5, X6, y6


def gen_lin_separable_overlap_data():
    """ Generate training data in the 2-d case. """
    mean1 = np.array([0, 2])
    mean2 = np.array([2, 0])
    cov = np.array([[1.5, 1.0], [1.0, 1.5]])
    X1 = np.random.multivariate_normal(mean1, cov, 100)
    y1 = np.ones(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov, 100)
    y2 = np.ones(len(X2)) * (-1)
    return X1, y1, X2, y2


def split_train(X1, y1, X2, y2):
    """ Split train data in 2-class 2-D case. """
    n = np.shape(y1)[0]
    # Keeping 80% of the data for training
    m = int(0.80 * n)
    X1_train = X1[:m]
    y1_train = y1[:m]
    X2_train = X2[:m]
    y2_train = y2[:m]
    X_train = np.vstack((X1_train, X2_train))
    y_train = np.hstack((y1_train, y2_train))
    return X_train, y_train


def split_test(X1, y1, X2, y2):
    """ Split test data in 2-class 2-D case. """
    n = np.shape(y1)[0]
    # Keeping 80% of the data for training
    m = int(0.80 * n)
    X1_test = X1[m:]
    y1_test = y1[m:]
    X2_test = X2[m:]
    y2_test = y2[m:]
    X_test = np.vstack((X1_test, X2_test))
    y_test = np.hstack((y1_test, y2_test))
    return X_test, y_test


def split_train_3class(X1, y1, X2, y2, X3, y3):
    """ Split train data in 3-class 2-D case. """
    n = np.shape(y1)[0]
    # Keeping 80% of the data for training
    m = int(0.80 * n)
    X1_train = X1[:m]
    y1_train = y1[:m]
    X2_train = X2[:m]
    y2_train = y2[:m]
    X3_train = X3[:m]
    y3_train = y3[:m]
    X_train = np.vstack((X1_train, X2_train, X3_train))
    y_train = np.hstack((y1_train, y2_train, y3_train))
    return X_train, y_train


def split_test_3class(X1, y1, X2, y2, X3, y3):
    """ Split test data in 3-class 2-D case. """
    n = np.shape(y1)[0]
    # Keeping 80% of the data for training
    m = int(0.80 * n)
    X1_test = X1[m:]
    y1_test = y1[m:]
    X2_test = X2[m:]
    y2_test = y2[m:]
    X3_test = X3[m:]
    y3_test = y3[m:]
    X_test = np.vstack((X1_test, X2_test, X3_test))
    y_test = np.hstack((y1_test, y2_test, y3_test))
    return X_test, y_test


def split_train_6class(X1, y1, X2, y2, X3, y3, X4, y4, X5, y5, X6, y6):
    """ Split train data in 6-class 2-D case. """
    n = np.shape(y1)[0]
    # Keeping 50% of the data for training
    m = int(0.50 * n)
    X1_train = X1[:m]
    y1_train = y1[:m]
    X2_train = X2[:m]
    y2_train = y2[:m]
    X3_train = X3[:m]
    y3_train = y3[:m]
    X4_train = X4[:m]
    y4_train = y4[:m]
    X5_train = X5[:m]
    y5_train = y5[:m]
    X6_train = X6[:m]
    y6_train = y6[:m]
    X_train = np.vstack((X1_train, X2_train, X3_train, X4_train, X5_train, X6_train))
    y_train = np.hstack((y1_train, y2_train, y3_train, y4_train, y5_train, y6_train))
    return X_train, y_train


def split_test_6class(X1, y1, X2, y2, X3, y3, X4, y4, X5, y5, X6, y6):
    """ Split test data in 6-class 2-D case. """
    n = np.shape(y1)[0]
    # Keeping 50% of the data for training
    m = int(0.50 * n)
    X1_test = X1[m:]
    y1_test = y1[m:]
    X2_test = X2[m:]
    y2_test = y2[m:]
    X3_test = X3[m:]
    y3_test = y3[m:]
    X4_test = X4[m:]
    y4_test = y4[m:]
    X5_test = X5[m:]
    y5_test = y5[m:]
    X6_test = X6[m:]
    y6_test = y6[m:]
    X_test = np.vstack((X1_test, X2_test, X3_test, X4_test, X5_test, X6_test))
    y_test = np.hstack((y1_test, y2_test, y3_test, y4_test, y5_test, y6_test))
    return X_test, y_test


GLOBAL_CONST_NUM_FIGURE = 0


def plot_margin(X1_train, X2_train, clf):
    """ Plot the margin based on separating hyperplane, for 2 class data. """
    def f(x, w, b, c=0):
        """ Given x, return y such that [x, y] in on the line w.x + b = c. """
        return (-w[0] * x - b + c) / w[1]
    # Start by plotting the point
    pl.plot(X1_train[:, 0], X1_train[:, 1], "ro")
    pl.plot(X2_train[:, 0], X2_train[:, 1], "bo")
    pl.scatter(clf.sv[:, 0], clf.sv[:, 1], s=100, c="g")
    # w.x + b = 0
    a0 = -4
    a1 = f(a0, clf.w, clf.b)
    b0 = 4
    b1 = f(b0, clf.w, clf.b)
    pl.plot([a0, b0], [a1, b1], "k")
    # w.x + b = 1
    a0 = -4
    a1 = f(a0, clf.w, clf.b, 1)
    b0 = 4
    b1 = f(b0, clf.w, clf.b, 1)
    pl.plot([a0, b0], [a1, b1], "k--")
    # w.x + b = -1
    a0 = -4
    a1 = f(a0, clf.w, clf.b, -1)
    b0 = 4
    b1 = f(b0, clf.w, clf.b, -1)
    pl.plot([a0, b0], [a1, b1], "k--")
    # Finally, we are done
    pl.axis('tight')
    pl.show()


def plot_contour(X1_train, X2_train, clf):
    """ Plot the contour, for 2 class data. """
    pl.plot(X1_train[:, 0], X1_train[:, 1], "ro")
    pl.plot(X2_train[:, 0], X2_train[:, 1], "bo")
    pl.scatter(clf.sv[:, 0], clf.sv[:, 1], s=100, c="g")
    n = 50
    X1, X2 = np.meshgrid(np.linspace(-6, 6, n), np.linspace(-6, 6, n))
    X = np.array([[x1, x2] for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
    Z = clf.project(X).reshape(X1.shape)
    pl.contour(X1, X2, Z, [0.0], colors='k', linewidths=1, origin='lower')
    pl.contour(X1, X2, Z + 1, [0.0], colors='grey', linewidths=1, origin='lower')
    pl.contour(X1, X2, Z - 1, [0.0], colors='grey', linewidths=1, origin='lower')
    # Done
    pl.axis('tight')
    pl.show()


def plot_contour_3class(X1_train, X2_train, X3_train, clf):
    """ Plot the contour, for 3 class data. """
    pl.plot(X1_train[:, 0], X1_train[:, 1], "ro")
    sv_y = clf._binary_SVCs[0].sv_y  # Find support vectors labels for this BinarySVC
    sv = clf._binary_SVCs[0].sv  # Find support vectors for this BinarySVC
    sv = sv[sv_y > 0]  # Only keep the support vectors IN this class
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="r")

    pl.plot(X2_train[:, 0], X2_train[:, 1], "bo")
    sv_y = clf._binary_SVCs[1].sv_y  # Find support vectors labels for this BinarySVC
    sv = clf._binary_SVCs[1].sv  # Find support vectors for this BinarySVC
    sv = sv[sv_y > 0]  # Only keep the support vectors IN this class
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="b")

    pl.plot(X3_train[:, 0], X3_train[:, 1], "go")
    sv_y = clf._binary_SVCs[2].sv_y  # Find support vectors labels for this BinarySVC
    sv = clf._binary_SVCs[2].sv  # Find support vectors for this BinarySVC
    sv = sv[sv_y > 0]  # Only keep the support vectors IN this class
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="g")

    n = 50
    X1, X2 = np.meshgrid(np.linspace(-8, 8, n), np.linspace(-8, 8, n))
    X = np.array([[x1, x2] for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
    Z = clf.project(X).reshape((3, n, n))
    for k in range(3):
        pl.contour(X1, X2, Z[k], [0.0], colors='k', linewidths=1, origin='lower')
        pl.contour(X1, X2, Z[k] + 1, [0.0], colors='grey', linewidths=1, origin='lower')
        pl.contour(X1, X2, Z[k] - 1, [0.0], colors='grey', linewidths=1, origin='lower')
    # Done
    pl.axis('tight')
    pl.show()


def plot_contour_6class(X1_train, X2_train, X3_train, X4_train, X5_train, X6_train, clf):
    """ Plot the contour, for 6 class data. """
    pl.plot(X1_train[:, 0], X1_train[:, 1], "ro")
    sv_y = clf._binary_SVCs[0].sv_y
    sv = clf._binary_SVCs[0].sv
    sv = sv[sv_y > 0]
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="r")

    pl.plot(X2_train[:, 0], X2_train[:, 1], "bo")
    sv_y = clf._binary_SVCs[1].sv_y
    sv = clf._binary_SVCs[1].sv
    sv = sv[sv_y > 0]
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="b")

    pl.plot(X3_train[:, 0], X3_train[:, 1], "go")
    sv_y = clf._binary_SVCs[2].sv_y
    sv = clf._binary_SVCs[2].sv
    sv = sv[sv_y > 0]
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="g")

    pl.plot(X4_train[:, 0], X4_train[:, 1], "co")
    sv_y = clf._binary_SVCs[3].sv_y
    sv = clf._binary_SVCs[3].sv
    sv = sv[sv_y > 0]
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="c")

    pl.plot(X5_train[:, 0], X5_train[:, 1], "mo")
    sv_y = clf._binary_SVCs[4].sv_y
    sv = clf._binary_SVCs[4].sv
    sv = sv[sv_y > 0]
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="m")

    pl.plot(X6_train[:, 0], X6_train[:, 1], "yo")
    sv_y = clf._binary_SVCs[5].sv_y
    sv = clf._binary_SVCs[5].sv
    sv = sv[sv_y > 0]
    pl.scatter(sv[:, 0], sv[:, 1], s=100, c="y")

    n = 50
    X1, X2 = np.meshgrid(np.linspace(-8, 8, n), np.linspace(-8, 8, n))
    X = np.array([[x1, x2] for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
    Z = clf.project(X).reshape((6, n, n))
    for k in range(6):
        pl.contour(X1, X2, Z[k], [0.0], colors='k', linewidths=1, origin='lower')
        pl.contour(X1, X2, Z[k] + 1, [0.0], colors='grey', linewidths=1, origin='lower')
        pl.contour(X1, X2, Z[k] - 1, [0.0], colors='grey', linewidths=1, origin='lower')
    # Done
    pl.axis('tight')
    pl.show()


def test_linear(C=1, use_smo=False):
    """ Test linear SVC, hard margin (C = FIXME ?). """
    global GLOBAL_CONST_NUM_FIGURE

    X1, y1, X2, y2 = gen_lin_separable_data()
    X_train, y_train = split_train(X1, y1, X2, y2)
    X_test, y_test = split_test(X1, y1, X2, y2)

    clf = BinarySVC(kernel='linear', C=C, use_smo=use_smo, verbose=1)
    print(clf)
    clf.fit(X_train, y_train)
    print(clf)

    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print("%d out of %d predictions correct" % (correct, len(y_predict)))

    GLOBAL_CONST_NUM_FIGURE += 1
    print("Opening figure numbered", GLOBAL_CONST_NUM_FIGURE)
    pl.figure(GLOBAL_CONST_NUM_FIGURE)
    pl.title("Test of hard-margin SVM with linear kernel")
    plot_margin(X_train[y_train == 1], X_train[y_train == -1], clf)
    # pl.savefig("Test_of_hard-margin_SVM_with_linear_kernel.png")


def test_non_linear(C=2, use_smo=False):
    """ Test RBF-kernel SVC, with soft margin, C = 2 """
    global GLOBAL_CONST_NUM_FIGURE

    X1, y1, X2, y2 = gen_non_lin_separable_data()
    X_train, y_train = split_train(X1, y1, X2, y2)
    X_test, y_test = split_test(X1, y1, X2, y2)

    gamma = 0.1
    clf = BinarySVC(kernel='rbf', C=C, gamma=gamma, use_smo=use_smo, verbose=1)
    print(clf)
    clf.fit(X_train, y_train)
    print(clf)

    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print("%d out of %d predictions correct" % (correct, len(y_predict)))

    GLOBAL_CONST_NUM_FIGURE += 1
    print("Opening figure numbered", GLOBAL_CONST_NUM_FIGURE)
    pl.figure(GLOBAL_CONST_NUM_FIGURE)
    pl.title("Test of soft-margin SVM C={} with RBF kernel (Gaussian)".format(C))
    plot_contour(X_train[y_train == 1], X_train[y_train == -1], clf)
    # pl.savefig("Test_of_soft-margin_SVM_with_RBF_kernel__C={}_gamma={}.png".format(C, gamma))


def test_soft(C=1, use_smo=False):
    """ Test linear SVC, with soft margin, C = 0.1 """
    global GLOBAL_CONST_NUM_FIGURE

    X1, y1, X2, y2 = gen_lin_separable_overlap_data()
    X_train, y_train = split_train(X1, y1, X2, y2)
    X_test, y_test = split_test(X1, y1, X2, y2)

    clf = BinarySVC(kernel='linear', C=C, use_smo=use_smo, verbose=1)
    print(clf)
    clf.fit(X_train, y_train)
    print(clf)

    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print("%d out of %d predictions correct" % (correct, len(y_predict)))

    GLOBAL_CONST_NUM_FIGURE += 1
    print("Opening figure numbered", GLOBAL_CONST_NUM_FIGURE)
    pl.figure(GLOBAL_CONST_NUM_FIGURE)
    pl.title("Test of soft-margin SVM with C={} linear kernel".format(C))
    plot_contour(X_train[y_train == 1], X_train[y_train == -1], clf)
    # pl.savefig("Test_of_soft-margin_SVM_with_linear_kernel.png")


def test_non_linear_3_class(kernel='rbf', C=1, gamma=0.5, degree=1, use_smo=False, n_jobs=1):
    """ Test RBF-kernel SVC for 3 classes, with soft margin, C = 1 """
    global GLOBAL_CONST_NUM_FIGURE

    X1, y1, X2, y2, X3, y3 = gen_non_lin_separable_data_3class()
    X_train, y_train = split_train_3class(X1, y1, X2, y2, X3, y3)
    # y_train -= 1
    X_test, y_test = split_test_3class(X1, y1, X2, y2, X3, y3)
    # y_test -= 1

    if kernel == 'poly':
        k_name = "{} degree={}".format(kernel.title(), degree)
    elif kernel == 'rbf':
        k_name = "{} gamma={}".format(kernel.upper(), gamma)
    else:
        k_name = "{} gamma={}".format(kernel.title(), gamma)
    print("\n\nFor the kernel", k_name)
    clf = mySVC(kernel=kernel, C=C, gamma=gamma, degree=degree, verbose=1, use_smo=use_smo, n_jobs=n_jobs)
    clf.fit(X_train, y_train)
    print(clf)
    # return clf  # XXX to only measure the time required to train the model
    # Predict
    start_time = time()
    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print("%d out of %d predictions correct" % (correct, len(y_predict)))
    # Display
    GLOBAL_CONST_NUM_FIGURE += 1
    print("Opening figure numbered", GLOBAL_CONST_NUM_FIGURE)
    pl.figure(GLOBAL_CONST_NUM_FIGURE)
    pl.title("Test of 3-class soft-margin SVM C={} with {} kernel".format(C, k_name))
    plot_contour_3class(X_train[y_train == 0], X_train[y_train == 1], X_train[y_train == 2], clf)
    # pl.savefig("Test_of_3-class_soft-margin_SVM_with_{}_kernel__C={}_gamma={}.png".format(k_name, C, gamma))
    delta = time() - start_time
    print("===>   For prediction, estimated running time: {:g} seconds ...".format(delta))


def test_non_linear_6_class(kernel='rbf', C=1, gamma=0.5, degree=1, use_smo=False, n_jobs=1):
    """ Test RBF-kernel SVC for 6 classes, with soft margin, C = 1 """
    global GLOBAL_CONST_NUM_FIGURE

    X1, y1, X2, y2, X3, y3, X4, y4, X5, y5, X6, y6 = gen_non_lin_separable_data_6class()
    X_train, y_train = split_train_6class(X1, y1, X2, y2, X3, y3, X4, y4, X5, y5, X6, y6)
    # y_train -= 1
    X_test, y_test = split_test_6class(X1, y1, X2, y2, X3, y3, X4, y4, X5, y5, X6, y6)
    # y_test -= 1

    if kernel == 'poly':
        k_name = "{} degree={}".format(kernel.title(), degree)
    elif kernel == 'rbf':
        k_name = "{} gamma={}".format(kernel.upper(), gamma)
    else:
        k_name = "{} gamma={}".format(kernel.title(), gamma)
    print("\n\nFor the kernel", k_name)
    clf = mySVC(kernel=kernel, C=C, gamma=gamma, degree=degree, verbose=1, use_smo=use_smo, n_jobs=n_jobs)
    clf.fit(X_train, y_train)
    print(clf)
    # return clf  # XXX to only measure the time required to train the model
    # Predict
    start_time = time()
    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print("%d out of %d predictions correct" % (correct, len(y_predict)))
    # Display
    GLOBAL_CONST_NUM_FIGURE += 1
    print("Opening figure numbered", GLOBAL_CONST_NUM_FIGURE)
    pl.figure(GLOBAL_CONST_NUM_FIGURE)
    pl.title("Test of 6-class soft-margin SVM C={} with {} kernel".format(C, k_name))
    plot_contour_6class(X_train[y_train == 0], X_train[y_train == 1], X_train[y_train == 2], X_train[y_train == 3], X_train[y_train == 4], X_train[y_train == 5], clf)
    # pl.savefig("Test_of_6-class_soft-margin_SVM_C={}__with_{}_kernel.png".format(C, k_name))
    delta = time() - start_time
    print("===>   For prediction, estimated running time: {:g} seconds ...".format(delta))


if __name__ == "__main__":
    # Run the tests on my class
    from svm import BinarySVC
    from svm import mySVC
    test_linear()
    # for use_smo in [False, True]:
    #     test_linear(use_smo=use_smo)
    #     # test_soft(use_smo=use_smo)
    #     # test_non_linear(use_smo=use_smo)
    #     # test_non_linear_3_class(use_smo=use_smo)
    #     # test_non_linear_6_class(use_smo=use_smo)
    #     # print(input("[Enter to continue]"))
    test_non_linear()
    test_soft()
    # for kernel in ['rbf', 'laplace', 'linear']:
    #     test_non_linear_3_class(kernel=kernel)
    # for degree in [1, 2, 3]:
    #     test_non_linear_3_class(kernel='poly', degree=degree)
    # deltas = {}
    # for n_jobs in [1, 2, 3, -1]:
    #     print("\n\n\nStarting test_non_linear_3_class with n_jobs =", n_jobs, "...")
    #     start_time = time()
    #     test_non_linear_3_class(kernel='rbf', n_jobs=n_jobs)
    #     delta = time() - start_time
    #     print("\nWith n_jobs = {}, estimated running time: {:g} seconds ...".format(n_jobs, delta))
    #     deltas[n_jobs] = round(delta, 3)
    #     print(input("[Enter to continue]"))
    # print("\n\ndeltas time =", deltas)
    # for kernel in ['rbf', 'laplace', 'linear']:
    #     test_non_linear_6_class(kernel=kernel)
    # for degree in [2, 3]:
    #     test_non_linear_6_class(kernel='poly', degree=degree)
    # for degree in [4, 5]:
    #     test_non_linear_6_class(kernel='poly', degree=degree, C=0.1)
    # deltas = {}
    # for n_jobs in [1, 2, 3, 6, -1]:
    #     print("\n\n\nStarting test_non_linear_6_class with n_jobs =", n_jobs, "...")
    #     start_time = time()
    #     test_non_linear_6_class(kernel='rbf', n_jobs=n_jobs)
    #     delta = time() - start_time
    #     print("\nWith n_jobs = {}, estimated running time: {:g} seconds ...".format(n_jobs, delta))
    #     deltas[n_jobs] = round(delta, 3)
    #     # print(input("[Enter to continue]"))
    # print("\n\ndeltas time =", deltas)
    print("Done testing all these kernels !")


print("Estimated running time: {:g} seconds ...".format(time() - start_time_0))
print("==> Done for this script svm.py")

# End of svm.py
